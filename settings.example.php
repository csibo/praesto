<?php

namespace Praesto;

$_SETTINGS = array
(

	// Database settings.
    'databases' => array
	(
		
		// Database 1.
		'demo' => array
		(
			
			// Connection settings.
			'host' => 'localhost', 					// Typically "localhost".
			'database' => '', 						// Database name.
			'username' => '', 						// Username for connection.
			'password' => '', 						// Password for connection.
			
			// Settings for Praesto->DB interfacing.
			'database settings' => array
			(
				// Table containing the possible tags.
				'tag table' => array
				(
				
					'name' => 'tags', 				// Table name.
					'id column' => 'id', 			// Name of primary key column.
					'data column' => 'tag', 		// Name of tag value column.
					'data is delimited' => true, 	// Whether tags are stored as multi-word values or not. ie. false = "multi_word", true = "multi word".
					
				),
				
				// Table containing the tagged data (ie. images, posts, etc).
				'item table' => array
				(
				
					'name' => 'items', 				// Table name.
					'id column' => 'id', 			// Name of primary key column.
					
				),
				
				// Table containing the relationship between tags and tagged data.
				'tag-item table' => array
				(
				
					'name' => 'item_tags', 			// Table name.
					'item column' => 'item_id', 	// Name of the tagged data ID column.
					'tag column' => 'tag_id',		// Name of the tag ID column.
					
				),
				
			),
			
		),
		
    ),
	
	
	
	// General Praesto settings.
	'search' => array
	(
	
		'delimiter'					=> ' ', 		// Search term delimiter. Ex. Term1 Term2
		'fuzzy search ratio' 		=> false, 		// True = Use (term length / max levenshtein distance). False = Use (max levenshtein distance).
		'max levenshtein distance' 	=> 2, 			// Max distance between fuzzy tag and resultant tags. Ex. ~rest -> test = distance of 1
		'fail on missing tag'		=> true,		// Whether to return no results if requesting a tag that doesn't exist. If false, query will ignore non-existent tags.
		'max number of terms'		=> 10,			// Max number of terms that a query can have. Any extra will be ignored. NB. "tag" and "tag1|tag2|tag3" each count as 1 term.
		'min tag length'			=> 3,			// Min character count for each tag. Any shorter tags will be ignored.
	
	),
	
	
	
	// Praesto term settings.
	'terms' => array
	(
		
		'word delimiter' 	=> '_',					// Intra-tag word delimiter. Ex. multi-word_search_term
		
		
		
		// Search mode tokens.
		'tokens' => array
		(
			
			'exclude' 	=> '-', 					// Token to exclude a tag.
			'include'	=> '+',						// Token to include a tag (Usually redundant).
			'fuzzy' 	=> '~',						// Token to perform a fuzzy search.
			'or' 		=> '|',						// Token to denote an inclusive-OR group.
			'query' 	=> ':',						// Token to delimit query modifiers.
			
		),
		
		
		
		// Tag blacklist. Any tags here will be removed from queries.
		// NB. This is not the same as an exclusion tag (eg. -term). This will
		// completely ignore a tag to avoid any processing on it.
		'blacklist'			=> array
		(
		
			//'the',
		
		),
		
		
		
		// Tag whitelist. Any tags here will always be allowed. 
		// For instance, this is useful for overriding the minimum tag length.
		// NB. This will not circumvent the maximum number of tags.
		'whitelist'			=> array
		(
		
			//'3d',
		
		),
		
	),

);