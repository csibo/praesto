<?php

class DatabaseHandler
{
	private $_SETTINGS;

    protected $db;
    protected $database;
    protected $hostname;
    protected $username;
	
	public $queryCount;
    
	// Default constructor.
    public function __construct()
    {
		global $Diagnostics, $_SETTINGS;
		
		if($_SETTINGS == null)
		{
			// Retrieve our settings file.
			require(__DIR__.'/../settings.php');
		}
		
		$this->_SETTINGS = $_SETTINGS;
		
		$this->queryCount = 0;
		
		if(isset($Diagnostics))
		{
			$Diagnostics->databases[] = &$this;
		}
    }
    
    public function __set($name, $value)
    {
        // Make sure nothing overwrites our connection settings.
        // Everything else is fair game.
        switch($name)
        {
            case 'hostname':
            case 'username':
            case 'database':
            case 'db_connection':
                break;
            default:
                $this->$name = $value;
                break;
        }
    }
    
    public function __get($name)
    {
        return $this->$name;
    }
    
	// Connect to specified database.
    public function Connect($database)
    {
        // Make sure everything gets reset.
		$this->Disconnect();
	
        try
        {
			// Retrieve connection settings for specified database.
			$settings = $this->_SETTINGS['databases'][$database];
		
            // Create a new connection with the supplied settings.
            $this->db = new \PDO
			(
				'mysql:host=' . $settings['host'] . ';' .
				'dbname=' . $settings['database'], 
				$settings['username'], 
				$settings['password']
			);
            
            // Store our connection settings for future use.
            $this->database = $settings['database'];
            $this->hostname = $settings['host'];
            $this->username = $settings['username'];
            
			// Report success.
            return true;
        }
        catch(\PDOException $e) 
        {
			// Display error.
            echo $e;
            
            // Make sure everything gets reset.
            $this->Disconnect();
        }
        
		// Report failure.
        return false;
    }
    
	// Reset all connection settings.
    public function Disconnect()
    {
        // Disconnect from the database
        $this->db = null;
        
        // Reset all our connection settings
        $this->database = null;
        $this->hostname = null;
        $this->username = null;
        
        return true;
    }
    
	// Shortcut for PDO->db->prepare()
    public function Prepare(&$query)
    {
        try
        {
            return $this->db->prepare($query);
        }
        catch(\PDOException $e) 
        {
            echo $e;
        }
    }
	
    // Shortcut for PDO->db->execute()
    public function Execute(&$query, &$arguments = null)
    {
		$this->queryCount++;
		
        try
        {
            $query = $this->Prepare($query);

            $query->execute($arguments);
			
			return $query;
        }
        catch(\PDOException $e) 
        {
            echo $e;
        }
    }
	
    // Shortcut for PDO->db->fetch()
    public function Fetch($query_string, $arguments = null)
    {
        try
        {
            $query = $this->Execute($query_string, $arguments);

            return $query->fetch(\PDO::FETCH_ASSOC);
        }
        catch(\PDOException $e) 
        {
            echo $e;
        }
    }
	
    // Shortcut for PDO->db->fetch()
    public function FetchAndFlatten($query_string, $arguments = null)
    {
        $results = $this->Fetch($query_string, $arguments);
		
		if($results)
		{
			return $results[key($results)];
		}
		
		return false;
    }
	
    // Shortcut for PDO->db->fetchAll()
    public function FetchAll($query_string, $arguments = null)
    {
        try
        {
            $query = $this->Execute($query_string, $arguments);

            return $query->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch(\PDOException $e) 
        {
            echo $e;
        }
    }
	
    public function FetchAllAndGroup($query_string, $arguments, $group)
    {
        $results = $this->FetchAll($query_string, $arguments);
		
		$ordered_results = array();
		
		foreach($results as $index => $result)
		{
			$ordered_results[$result[$group]][] = $result;
		}
		
		return $ordered_results;
    }
	
    public function FetchAllAndFlatten($query_string, $arguments = null)
    {
        $results = $this->FetchAll($query_string, $arguments);
		
		$flattened_results = array();
		
		$key = key($results[0]);
		
		foreach($results as $result)
		{
			$flattened_results[] = $result[$key];
		}
		
		unset($results);
		
		return $flattened_results;
    }
	
    // Shortcut for PDO->db->exec()
	public function Exec($query_string)
	{
		$this->queryCount++;
		
		try
		{
            $this->db->beginTransaction();
			
			$sth = $this->db->exec($query_string);
			
			$this->db->commit();
		}
		catch(\PDOException $e)
		{
			echo $e;
		}
	}
	
    // Shortcut for PDO->db->errorInfo()
	public function ErrorInfo()
	{
		return $this->db->errorInfo();
	}
	
	// Shortcut for PDO->db->lastInsertId()
	public function LastInsertId()
	{
		return $this->db->lastInsertId();
	}
	
	public function ColumnExists($table, $column)
	{
		return !empty($this->FetchAll('SHOW COLUMNS FROM ' . $table . ' LIKE "' . $column . '"'));
	}
	
	public static function RenderAsTable($results, $maxRows = PHP_INT_MAX, $includeStyle = true)
	{
		if(empty($results) || !is_array($results))
		{
			return null;
		}
		
		$style = '<style>
		
			.DatabaseHandler_GeneratedTable
			{
				width:100%;
				text-align:center;
				border:1px solid rgb(60,60,60);
				background-color:rgba(220,220,220,0.8);
				border-radius:5px;
				font-family:monospace;
				font-size: 15px;
			}
		
			.DatabaseHandler_GeneratedTable tr:nth-child(even)
			{
				background-color:rgba(0,0,0,0.1);
			}
			
			.DatabaseHandler_GeneratedTable th
			{
				border-bottom:1px solid black;
				font-size: 17px;
				font-weight:bold;
			}
		
		</style>';
		
		$table = '<table class="DatabaseHandler_GeneratedTable" id="' . uniqid() . '">';
		
		if(is_array($results[0]))
		{
			foreach($results as $index => $row)
			{
				$table .= '<tr>';
				
				foreach($row as $column => $value)
				{
					$table .= '<th>';
					
					$table .= ucwords($column);
					
					$table .= '</th>';
				}
				
				$table .= '</tr>';
				
				break;
			}
			
			$isTruncated = count($results) > $maxRows;
			$originalCount = count($results);
			
			$results = array_slice($results, 0, $maxRows);
		
			foreach($results as $index => $row)
			{
				$table .= '<tr>';
				
				foreach($row as $column => $value)
				{
					$table .= '<td>';
					
					$table .= $value;
					
					$table .= '</td>';
				}
				
				$table .= '</tr>';
			}
			
			if($isTruncated)
			{
				$table .= '<tr><td colspan="99">... Omitted ' . ($originalCount - count($results)) . ' row' . (($originalCount - count($results)) == 1 ? '' : 's') . ' ...</td></tr>';
			}
		}
		else
		{
			$table .= '<tr>';
				
			$table .= '<th>ID</th>';
			
			$table .= '<th>Value</th>';
			
			$table .= '</tr>';
			
			foreach($results as $index => $row)
			{
				$table .= '<tr>';
				
				$table .= '<td>' . $index . '</td>';
				
				$table .= '<td>' . $row . '</td>';
				
				$table .= '</tr>';
			}
		}
		
		$table .= '</table>';
		
		if($includeStyle)
		{
			$table = $style . $table;
		}
		
		return $table;
	}
}