<?php

namespace Praesto;



/**
* Processes individual search terms.
*
* @package    Praesto
* @author     Christian Sibo
* @version    1.0
*/
class QueryInterpreter
{
	
	public $rawQuery;
	public $queryTerms;
	public $queryModString;
	public $processedQuery;
	
	public $allTags;
	public $termsFound;
	public $termsProcessed;
	public $termsSkipped;
	public $tagsIncluded;
	public $tagsExcluded;
	public $missingTag;
	public $queryCommands;
	public $queryFilters;
	public $timeToIds;
	
	
	protected $Praesto;
	protected $tagTable;
	protected $itemTable;
	protected $tagItemTable;
	protected $DB;
	
	
	private $_SETTINGS;
	
	
	
	/**
	 * Constructor.
	 *
	 * @param 	Praesto &$Praesto 	Reference to the Praesto main object.
	 * @param 	string 	$query 		Raw search string.
	 */
	public function __construct(&$Praesto, $query = null)
	{
		global $_SETTINGS;
		
		$this->_SETTINGS = $_SETTINGS;
		
		$this->Praesto = &$Praesto;
		
		
		
		// Grab the database key from the Praesto object.
		$this->database = $this->Praesto->database;
		
		// Get the settings for the database and its tables.
		$dbSettings = $this->_SETTINGS['databases'][$this->database]['database settings'];
		
		$this->itemTable 	= $dbSettings['item table'];
		$this->tagTable 	= $dbSettings['tag table'];
		$this->tagItemTable = $dbSettings['tag-item table'];
		
		
		
		// Get a new database connection.
		$this->DB = new \DatabaseHandler();
		$this->DB->Connect($this->database);
		
		
		
		$this->Praesto->QueryInterpreter = &$this;
		
		
		
		// If the raw query is valid...
		if($this->SetQuery($query))
		{
			// Process the query to get the terms.
			return $this->ProcessQuery();
		}
	}
	
	
	
	/**
	 * Sets the raw query.
	 *
	 * @param 	string 	$query 	User's raw query.
	 * @return 	bool 			True if success.
	 */
	public function SetQuery($query)
	{
		if
		(
			!empty($query)
			&& is_string($query)
		)
		{
			// Save the query for later.
			$this->rawQuery = trim($query);
			
			// Return success.
			return true;
		}
		
		// Return failure.
		return false;
	}
	
	
	
	/**
	 * Processes the query to get the terms.
	 *
	 * @return 	bool 			True if success.
	 */
	public function ProcessQuery()
	{
		// If the query is not set...
		if(empty($this->rawQuery))
		{
			// Return failure.
			return false;
		}
		
		
		
		// Clear out the resultant array.
		$this->processedQuery = array();
		
		$this->termsSkipped = array();
		
		$this->missingTag = false;
		
		
		
		// Get the array of query elements.
		$queryArray = $this->GetQueryArray();
		
		
		
		// For each search term...
		foreach($queryArray as $item)
		{
			// Create and process a new Term.
			$term = new QueryTerm($this->Praesto, $item);
			
			
			
			// If the term reported that the tag doesn't exist...
			if($this->missingTag)
			{
				// Add this term to the skipped array.
				//$this->SkipTerm($term->termBody, 'Tag doesn\'t exist.', 'Term Pass');
				
				// If we are to fail on missing tags...
				if($this->_SETTINGS['search']['fail on missing tag'])
				{
					// Clear the memory.
					unset($term);
					
					// Stop processing and report failure.
					return false;
				}
				
				// Reset the flag for any further tags.
				$this->missingTag = false;
			}
			
			
			
			// If the term body is shorter than the minimum allowed
			// and the term isn't in the global whitelist...
			if
			(
					strlen($term->termBody) < $this->_SETTINGS['search']['min tag length']
				&& 	!self::IsWhitelisted($term->termBody)
			)
			{
				// Add this term to the skipped array.
				//$this->SkipTerm($term->termBody, 'Too short.', 'Term Pass');
				
				// Clear the memory.
				unset($term);
				
				// Skip this term so it doesn't get processed.
				continue;
			}
			
			
			
			// Add the Term to the catchall category.
			$this->processedQuery['all'][] = $term;
			
			// If the term is not a query term...
			if(empty($term->queryTerms))
			{
				// Add the Term to its specific category.
				$this->processedQuery[($term->exclude) ? 'exclude' : 'include'][] = $term;
			}
			else
			{
				// Add the Term to the query term category.
				$this->processedQuery['query'][] = $term;
			}
		}
		
		
		
		// If we didn't get any inclusive terms...
		if(empty($this->processedQuery['include']))
		{
			// Report failure.
			return false;
		}
		
		
		
		$this->termsProcessed 	= $this->processedQuery['all'];
		$this->tagsIncluded 	= $this->processedQuery['include'];
		$this->tagsExcluded 	= $this->processedQuery['exclude'];
		
		
		
		if(!empty($this->termsProcessed))
		{
			// Extract the rawTerm from each of the 'all' terms.
			array_walk
			(
				$this->termsProcessed, 
				function(&$value, $key)
				{
					$value = $value->rawTerm;
				}
			);
		}
		
		$this->BuildTermSets($this->tagsIncluded);
		
		$this->BuildTermSets($this->tagsExcluded);
		
		
		
		unset($this->processedQuery['all']);
		
		
		
		// If we have query terms to process...
		if(!empty($this->processedQuery['query']))
		{
			$this->queryModString = '';
			
			foreach($this->processedQuery['query'] as $queryMod)
			{
				// If the query term has a qualifying element...
				// (ex. command such as 'sort' or filter such as 'user')
				if(!empty($queryMod->queryTerms['qualifier']))
				{
					// The qualifier determines what we need to do...
					switch($queryMod->queryTerms['qualifier'])
					{
						// Sort command sorts the results on the specified 
						// column and in the specified direction.
						case 'sort':
						{
							// Save the sort command string.
							$orderClause = $queryMod->queryModString;
							
							break;
						}
						
						// Limit command limits the number of results.
						case 'limit':
						{
							// Save the limit command string.
							$limitClause = $queryMod->queryModString;
							
							break;
						}
						
						// Offset command offsets the results.
						case 'offset':
						{
							// Save the offset command string.
							$offsetClause = $queryMod->queryModString;
							
							break;
						}
						
						// The default means we're dealing with a filter term 
						// rather than a command term.
						default:
						{
							$this->queryModString .= $queryMod->queryModString;
							
							break;
						}
					}
				}
			}
			
			
			
			// Add any query commands to the PDO query string.
			// This must happen after every thing else or we'll get a syntax error.
			$this->queryModString .= $orderClause . $limitClause . $offsetClause; 
		}
		
		
		
		if(empty($limitClause) && !$this->Praesto->limitOverride)
		{
			$this->queryModString .= ' LIMIT 50';
		}
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
		
		
		
		// Return success.
		return true;
	}
	
	
	
	/**
	 * Logs included or excluded terms.
	 */
	protected function BuildTermSets(&$terms)
	{
		if(!empty($terms))
		{
			// Extract the rawTerm from each of the terms.
			array_walk
			(
				$terms, 
				function(&$value, $key)
				{
					if(empty($value->childTerms))
					{
						$value = $value->rawTerm;
					}
					else
					{
						$string = '';
						
						foreach($value->childTerms as $childIndex => $child)
						{
							if($childIndex > 0)
							{
								$string .= ' OR ';
							}
							
							if(!empty($child->childTerms))
							{
								$string .= '(';
								
								foreach($child->childTerms as $childIndex2 => $child2)
								{
									if(is_array($child2))
									{
										foreach($child2 as $childIndex3 => $child3)
										{
											if($childIndex3 > 0)
											{
												$string .= ' OR ';
											}
											
											$string .= $child3->rawTerm;
										}
									}
									else
									{
										$string .= $child2->rawTerm;
									}
								}
								
								$string .= ')';
							}
							else
							{
								$string .= $child->rawTerm;
							}
						}
						
						$value = $string;
					}
				}
			);
		}
	}
	
	
	
	/**
	 * Retrieves the list of tag IDs.
	 *
	 * @return 	array 			Array of tag IDs.
	 */
	public function GetTagIds()
	{
		$includeTagIds = array();
		
		// For each of the inclusion terms...
		for($i = 0; $i < count($this->processedQuery['include']); $i++)
		{
			// If we don't have anything in the ID array yet...
			if(empty($includeTagIds))
			{
				// Just set the ID array to be this term's items.
				$includeTagIds = $this->processedQuery['include'][$i]->tagIds;
			}
			else
			{
				// If this term has any matching item IDs...
				if(!empty($this->processedQuery['include'][$i]->tagIds))
				{
					// Intersect the ID array with this term's items to remove anything that isn't common between them both.
					$includeTagIds = array_merge($includeTagIds, $this->processedQuery['include'][$i]->tagIds);
				}
			}
		}
		
		unset($this->processedQuery['include']);
		
		
		
		// If we ended up with no inclusive IDs...
		if(empty($includeTagIds))
		{
			// Report failure.
			return array();
		}
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
		
		
		
		// This will store our list of item IDs that need to be excluded.
		$excludeTagIds = array();
		
		// If we have exclusion terms...
		if(!empty($this->processedQuery['exclude']))
		{
			// For each of the exclusion terms...
			for($i = 0; $i < count($this->processedQuery['exclude']); $i++)
			{
				// If we don't have anything in the exclusion ID array yet...
				if(empty($excludeTagIds))
				{
					// Just set the exclusion ID array to be this term's items.
					$excludeTagIds = $this->processedQuery['exclude'][$i]->tagIds;
				}
				else
				{
					// Intersect the exclusion ID array with this term's items to 
					// implicitly remove anything that isn't common between them both.
					$excludeTagIds = array_merge($excludeTagIds, $this->processedQuery['exclude'][$i]->tagIds);
				}
				
				unset($this->processedQuery['exclude'][$i]);
			}
			
			// Subtract the exclusion array from the inclusion array.
			//$includeTagIds = array_diff($includeTagIds, $excludeTagIds);
			
			unset($this->processedQuery['exclude']);
		}
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
		
		return $includeTagIds;
	}
	
	
	
	/**
	 * Retrieves the list of IDs tagged with our tags.
	 *
	 * @return 	array 			Array of content IDs.
	 */
	public function GetItemIds()
	{
		// This will store our final list of item IDs that match the user's query.
		$includeItemIds = array();
		
		$ids = $this->GetTagIds();
			
		if(count($ids) == 1)
		{
			$itemTagIdsQuery =
				'SELECT
					' . $this->tagItemTable['item column'] . '
				FROM
					' . $this->tagItemTable['name'] . '
				WHERE
					' . $this->tagItemTable['tag column'] . ' = ' . $ids[0];

		}
		else
		{
			$itemTagIdsQuery =
				'SELECT
					' . $this->tagItemTable['item column'] . '
				FROM
					' . $this->tagItemTable['name'] . '
				WHERE
					' . $this->tagItemTable['tag column'] . ' IN (' . implode(',', $ids) . ')
				GROUP BY 
					' . $this->tagItemTable['item column'] . '
				HAVING 
					COUNT(*) = ' . count($ids);
		}


		
		$itemTagIdsQuery .= $this->queryModString;
		
		$includeItemIds = $this->DB->FetchAllAndFlatten($itemTagIdsQuery);
		
		return $includeItemIds;
	}
	
	
	
	/**
	 * Breaks the raw query string into the term array.
	 *
	 * @return 	array 			Array of query elements.
	 */
	protected function GetQueryArray()
	{
		// Explode the raw query based on the term delimiter.
		$queryArray = explode($this->_SETTINGS['search']['delimiter'], $this->rawQuery);
		
		// Remove empty terms (if the user put multiple delimiters in a row).
		$queryArray = array_filter($queryArray);
		
		$this->termsFound = $queryArray;
		
		
		
		// If we have more query terms than are allowed...
		if(count($queryArray) > $this->_SETTINGS['search']['max number of terms'])
		{
			// Get the overflow terms.
			$overflow = array_slice($queryArray, $this->_SETTINGS['search']['max number of terms'] + 1);
			
			// Add the overflow message to the overflow terms.
			array_walk
			(
				$overflow, 
				function(&$value, $key)
				{
					// Add this term to the skipped array.
					$this->SkipTerm($value, 'Too many terms.', 'Query Parsing Pass');
				}
			);
			
			unset($overflow);
			
			// Ignore the extra terms.
			$queryArray = array_slice($queryArray, 0, $this->_SETTINGS['search']['max number of terms']);
		}
		
		
		
		// Ensure we don't have any skipped keys.
		$queryArray = array_values($queryArray);
		
		
		
		// Iterate over the query elements found...
		for($i = 0; $i < count($queryArray); $i++)
		{
			// If the term string is in the blacklist...
			if(self::IsBlacklisted($queryArray[$i]))
			{
				// Add this term to the skipped array.
				$this->SkipTerm($queryArray[$i], 'Global blacklist.', 'Query Parsing Pass');
				
				// Remove it from the results array.
				unset($queryArray[$i]);
				
				continue;
			}
			
			
			
			// If the term is shorter than the minimum allowed
			// and the term isn't in the global whitelist...
			if
			(
					strlen($queryArray[$i]) < $this->_SETTINGS['search']['min tag length']
				&& 	!self::IsWhitelisted($queryArray[$i])
			)
			{
				// Add this term to the skipped array.
				$this->SkipTerm($queryArray[$i], 'Too short.', 'Query Parsing Pass');
				
				// Remove it from the results array.
				unset($queryArray[$i]);
				
				continue;
			}
		}
		
		
		
		// Ensure we don't have any skipped keys.
		$queryArray = array_values($queryArray);
		
		
		
		return $queryArray;
	}
	
	
	
	/**
	 * Add log data for skipped terms.
	 *
	 * @param 	string 	$termString 		String of the term or tag skipped.
	 * @param 	string 	$reason 			Reason for being skipped.
	 * @param 	string 	$pass 				Phase in which it was skipped.
	 */
	protected function SkipTerm($termString, $reason, $pass)
	{
		// Add this term to the skipped array.
		$this->termsSkipped[] = array
		(
			'term' 		=> $termString,
			'reason' 	=> $reason,
			'pass'		=> $pass
		);
	}
	
	
	
	/**
	 * Checks tag against the global blacklist.
	 *
	 * @param 	string 	$tag 				Tag to check.
	 *
	 * @return	bool						True if blacklisted.
	 */
	static public function IsBlacklisted($tag)
	{
		global $_SETTINGS;
		
		// Check the tag string against the blacklist...
		if(in_array($tag, $_SETTINGS['terms']['blacklist']))
		{
			return true;
		}
		
		return false;
	}
	
	
	
	/**
	 * Checks tag against the global whitelist.
	 *
	 * @param 	string 	$tag 				Tag to check.
	 *
	 * @return	bool						True if whitelisted.
	 */
	static public function IsWhitelisted($tag)
	{
		global $_SETTINGS;
		
		// Check the tag string against the whitelist...
		if(in_array($tag, $_SETTINGS['terms']['whitelist']))
		{
			return true;
		}
		
		return false;
	}

}