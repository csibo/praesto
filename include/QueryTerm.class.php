<?php

namespace Praesto;



/**
* Processes individual search terms.
*
* @package    Praesto
* @author     Christian Sibo
* @version    1.0
*/
class QueryTerm
{
	
	public $tagId;
	public $rawTerm;
	public $exclude;
	public $blacklisted;
	public $whitelisted;
	public $fuzzy;
	public $query;
	public $termBody;
	public $queryTerms;
	public $queryModString;
	public $childTerms;
	public $database;
	public $taggedItems;
	
	protected $Praesto;
	protected $tagTable;
	protected $itemTable;
	protected $tagItemTable;
	protected $DB;
	
	private $_SETTINGS;
	
	
	
	/**
	 * Constructor.
	 *
	 * @param 	Praesto &$Praesto 	Reference to the Praesto main object.
	 * @param 	string 	$term 		Raw search string term.
	 */
	public function __construct(&$Praesto, $term)
	{
		global $_SETTINGS;
		
		$this->_SETTINGS = $_SETTINGS;
		
		$this->Praesto = &$Praesto;
		
		
		
		// Grab the database key from the Praesto object.
		$this->database = $this->Praesto->database;
		
		// Get the settings for the database and its tables.
		$dbSettings = $this->_SETTINGS['databases'][$this->database]['database settings'];
		
		$this->itemTable 	= $dbSettings['item table'];
		$this->tagTable 	= $dbSettings['tag table'];
		$this->tagItemTable = $dbSettings['tag-item table'];
		
		
		
		// Get a new database connection.
		$this->DB = new \DatabaseHandler();
		$this->DB->Connect($this->database);
		
		
		
		// If the raw term is valid...
		if($this->SetTerm($term))
		{
			// Process the term to get the relevant item IDs.
			return $this->ProcessTerm();
		}
	}
	
	
	
	/**
	 * Sets the raw query term.
	 *
	 * @param 	string 	$term 	User's raw query term.
	 * @return 	bool 			True if success.
	 */
	public function SetTerm($term)
	{
		if
		(
			!empty($term)
			&& is_string($term)
		)
		{
			// Save the query term for later.
			$this->rawTerm = $term;
			
			// Return success.
			return true;
		}
		
		// Return failure.
		return false;
	}
	
	
	
	/**
	 * Processes the term to get the relevant item IDs.
	 *
	 * @return 	bool 			True if success.
	 */
	public function ProcessTerm()
	{
		// If the raw term isn't set...
		if(empty($this->rawTerm))
		{
			// Return failure.
			return false;
		}
		
		
		
		// Set up default values.
		$this->tagId 		= null;
		$this->exclude 		= false;
		$this->fuzzy 		= false;
		$this->query 		= false;
		$this->childTerms 	= null;
		
		
		
		// Extract the components of the term.
		// 		$matches['exclude'] will only be set if the exclude token is the first character.
		// 		$matches['fuzzy'] will only be set if the fuzzy token is the first or second character.
		// 		$matches['body'] will contain the rest of the term.
		preg_match
		(
			'/^' . 
				'(?P<exclude>' . $this->_SETTINGS['terms']['tokens']['exclude'] . ')?' . 
				'(?P<fuzzy>' . $this->_SETTINGS['terms']['tokens']['fuzzy'] . ')?' . 
				'(?P<body>.+)' . 
			'$/', 
			
			$this->rawTerm, 
			$matches
		);
		
		
		
		// If the term was actually empty...
		if(empty($matches['body']))
		{
			// Return failure.
			return false;
		}
		
		
		
		// Set the exclude flag if the exclude token matched.
		$this->exclude = !empty($matches['exclude']);
		
		// Set the fuzzy flag if the fuzzy token matched.
		$this->fuzzy = !empty($matches['fuzzy']);
		
		// Save the term body.
		$this->termBody = $matches['body'];
		
		// If exploding the term body on the Query token returns one element, set the query flag.
		$this->query = count($this->queryTerms = explode($this->_SETTINGS['terms']['tokens']['query'], $this->termBody)) > 1 ? true : false;
		
		// If the query flag is false...
		if(!$this->query)
		{
			// Re-null the queryTerms array.
			$this->queryTerms = null;
		}
		
		
		
		if(QueryInterpreter::IsBlacklisted($this->termBody))
		{
			$this->SkipTerm($this->termBody, 'Global Blacklist.', 'Term Pass');
			
			$this->blacklisted = QueryInterpreter::IsBlacklisted($this->termBody);
			
			return false;
		}
		
		
		
		$this->whitelisted = QueryInterpreter::IsWhitelisted($this->termBody);
		
		
		
		if
		(
				strlen($this->termBody) < $this->_SETTINGS['search']['min tag length']
			&& 	!$this->whitelisted
		)
		{
			$this->SkipTerm($this->termBody, 'Too Short.', 'Term Pass');
			
			return false;
		}
		
		
		
		// If multi-word tags are stored in the table with a different delimiter than
		// is used in the raw query sting...
		if($this->_SETTINGS['databases'][$this->Praesto->database]['database settings']['tag table']['data is delimited'])
		{
			// Replace the raw multi-word delimiters with spaces.
			$this->termBody = trim(str_replace($this->_SETTINGS['terms']['word delimiter'], ' ', $this->termBody));
		}
		
		
		
		if(!$this->query)
		{
			// If exploding the term body on the Or token returns more than one element...
			if(count($exploded = explode($this->_SETTINGS['terms']['tokens']['or'], $matches['fuzzy'] . $this->termBody)) > 1)
			{
				// We need to process each ORed term as a new Term object.
				foreach($exploded as $subTerm)
				{
					$this->childTerms[] = new QueryTerm($this->Praesto, $subTerm);
				}
			}
			else
			{
				// If the fuzzy flag was set...
				if($this->fuzzy)
				{
					// Perform the fuzzy search.
					$this->GetFuzzyTerms();
				}
				
				
				
				// If the term looks like a regex...
				if($this->IsRegex())
				{
					// Perform the regex search.
					$this->GetRegExTerms();
				}
			}
		}
		
		
		
		// We don't need this anymore.
		unset($matches);
		
		
		
		// If this is a query term parent...
		if($this->query)
		{
			$this->ProcessQueryTerm();
		}
		
		
		
		// If this is a regular 'ol tag...
		if(empty($this->childTerms) && !$this->query)
		{
			// Get the tag ID associated with this tag string.
			$this->GetTagId();
			
			
			
			// If this tag didn't exist and we're to fail on missing tags...
			if
			(
					$this->Praesto->missingTag
				&&	$this->_SETTINGS['search']['fail on missing tag']
			)
			{
				$this->SkipTerm($this->termBody, 'Tag Doesn\'t Exist.', 'Term Pass');
				
				// Skip any further processing.
				return;
			}
		}
		
		
		
		$this->GetTagIds();
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
		
		
		
		// Return success.
		return true;
	}
	
	
	
	/**
	 * Processes query terms into their representative strings.
	 */
	protected function ProcessQueryTerm()
	{
		// Reassign the query terms to their respective indices.
		foreach($this->queryTerms as $index => $term)
		{
			// The position of the sub-elements determines how we process them.
			switch($index)
			{
				// Qualifying term.
				case 0:
				{
					$this->queryTerms['qualifier'] = $term;
					
					break;
				}
				
				// Term body.
				case 1:
				{
					$this->queryTerms['term'] = $term;
					
					if(count($exploded = explode($this->_SETTINGS['terms']['tokens']['or'], $this->queryTerms['term'])) > 1)
					{
						$this->queryTerms['term'] = $exploded;
					}
					
					break;
				}
				
				// Optional direction.
				case 2:
				{
					$this->queryTerms['direction'] = $term;
					
					break;
				}
			}
			
			// Remove the duplicate values.
			unset($this->queryTerms[$index]);
		}
		
		
		
		$this->queryModString = '';
		
		
		
		// If the query term has a qualifying element...
		// (ex. command such as 'sort' or filter such as 'user')
		if(!empty($this->queryTerms['qualifier']))
		{
			// The qualifier determines what we need to do...
			switch($this->queryTerms['qualifier'])
			{
				// Sort command sorts the results on the specified 
				// column and in the specified direction.
				case 'sort':
				{
					// Skip this term if the item table doesn't have the specified column.
					if(!$this->DB->ColumnExists($this->itemTable['name'], $this->queryTerms['term'])) 
					{
						return;
					}
					
					// Save the sort command string.
					// Default to ascending order, if not specified in the query term.
					$this->queryModString = ' ORDER BY ' . $this->itemTable['name'] . '.' . $this->queryTerms['term'] . ' ' . (empty($this->queryTerms['direction']) ? 'asc' : $this->queryTerms['direction']);
					
					$this->Praesto->QueryInterpreter->queryCommands[] = array
					(
						'command' 	=> $this->queryTerms['qualifier'],
						'value' 	=> $this->queryTerms['term'],
						'direction' => empty($this->queryTerms['direction']) ? 'asc' : $this->queryTerms['direction']
					);
					
					break;
				}
				
				// Limit command limits the number of results.
				case 'limit':
				{
					// Save the limit command string.
					$this->queryModString = ' LIMIT ' . $this->queryTerms['term'];
					
					$this->Praesto->QueryInterpreter->queryCommands[] = array
					(
						'command' 	=> $this->queryTerms['qualifier'],
						'value' 	=> $this->queryTerms['term'],
						'direction' => ''
					);
					
					break;
				}
				
				// Offset command offsets the results.
				case 'offset':
				{
					// Save the offset command string.
					$this->queryModString = ' OFFSET ' . $this->queryTerms['term'];
					
					$this->Praesto->QueryInterpreter->queryCommands[] = array
					(
						'command' 	=> $this->queryTerms['qualifier'],
						'value' 	=> $this->queryTerms['term'],
						'direction' => ''
					);
					
					break;
				}
				
				// The default means we're dealing with a filter term 
				// rather than a command term.
				default:
				{
					// Skip this term if the item table doesn't have the specified column.
					if(!$this->DB->ColumnExists($this->itemTable['name'], $this->queryTerms['qualifier'])) 
					{
						continue;
					}
					
					// Add the filter condition to the PDO query string.
					$this->queryModString .= ' AND (';
					
					if(!is_array($this->queryTerms['term']))
					{
						$this->queryTerms['term'] = array($this->queryTerms['term']);
					}
					
					foreach($this->queryTerms['term'] as $queryTermIndex => $term)
					{
						if($queryTermIndex > 0)
						{
							$this->queryModString .= ' OR ';
						}
						
						$this->queryModString .= $this->itemTable['name'] . '.' . $this->queryTerms['qualifier'] . ' ';
					
						// If the term contains an explicit comparison operator...
						if(preg_match('/^([\<\>\=]+)(.+)/', $term, $matches))
						{
							// Separate the operator and value.
							$this->queryModString .= $matches[1] . ' "' . $matches[2] . '"';
							
							$this->Praesto->QueryInterpreter->queryFilters[] = array
							(
								'column' 		=> $this->queryTerms['qualifier'],
								'comparison' 	=> $matches[1],
								'value' 		=> $matches[2]
							);
						}
						// Else, use the equals operator...
						else
						{
							$this->queryModString .= '= "' . $term . '"';
							
							$this->Praesto->QueryInterpreter->queryFilters[] = array
							(
								'column' 		=> $this->queryTerms['qualifier'],
								'comparison' 	=> '=',
								'value' 		=> $term
							);
						}
					}
						
					$this->queryModString .= ')';
					
					break;
				}
			}
		}
	}
	
	
	/**
	 * Performs the fuzzy search on a term using the Damerau-Levenshtein distance.
	 */
	protected function GetFuzzyTerms()
	{
		// Will store the matching tags from the fuzzy search.
		$newTerms = array();
		
		
		
		// Get all the tags from the tags table.
		$this->Praesto->GetAllTags();
		
		
		
		// If we want to use the distance setting 
		// as a ratio of the raw term's length...
		if($this->_SETTINGS['search']['fuzzy search ratio'])
		{
			// The distance limit is the term's length over the distance setting.
			$limit = strlen($this->termBody) / $this->_SETTINGS['search']['max levenshtein distance'];
		}
		else
		{
			// The distance limit is the distance setting.
			$limit = $this->_SETTINGS['search']['max levenshtein distance'];
		}
		
		
		
		// For each tag in the table...
		foreach($this->Praesto->allTags as $compare)
		{
			// Get the Damerau-Levenshtein object.
			$DamerauLevenshtein = new \Oefenweb\DamerauLevenshtein\DamerauLevenshtein($this->termBody, $compare);
			
			// If the distance between the strings is <= the max distance...
			if($DamerauLevenshtein->getSimilarity() <= $limit)
			{
				// Create a new Term object with this tag and add it to our array.
				$newTerms[] = new QueryTerm($this->Praesto, $compare);
			}
			
			unset($DamerauLevenshtein);
		}
		
		
		
		// Add the new terms to the children list as an array.
		$this->childTerms[] = $newTerms;
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
	}
	
	
	
	/**
	 * Checks if a string looks like a regular expression.
	 *
	 * @return bool
	 */
	protected function IsRegex()
	{
		// Look for regex special tokens.
		if(preg_match('/[\.\+\{\}\[\]\?\|]+/', $this->termBody))
		{
			return true;
		}
		
		// Look for meta characters.
		if(preg_match('/\\\\\w/', $this->termBody))
		{
			return true;
		}
		
		return false;
	}
	
	
	
	/**
	 * Performs a regular expression search for tags.
	 */
	protected function GetRegExTerms()
	{
		// Will store the matching tags from the regex search.
		$newTerms = array();
		
		
		
		// Get all the tags from the tags table.
		$this->Praesto->GetAllTags();
		
		
		
		// For each tag in the table...
		foreach($this->Praesto->allTags as $compare)
		{
			if(QueryInterpreter::IsBlacklisted($compare))
			{
				$this->SkipTerm($compare, 'Global Blacklist.', 'Regex Term Pass');
				
				continue;
			}
			
			if(preg_match('@^' . $this->termBody . '$@', $compare))
			{
				$newTerms[] = new QueryTerm($this->Praesto, $compare);
			}
		}
		
		
		
		// Add the new terms to the OR list, since they'll
		// be processed similarly later on.
		$this->childTerms[] = $newTerms;
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
	}
	
	
	
	/**
	 * Retrieves the tags IDs for this term.
	 */
	public function GetTagIds()
	{
		// Will store the IDs of the tagged items.
		$this->tagIds = array();
		
		
		
		if(empty($this->childTerms))
		{
			if
			(
					$this->tagId == null
				||	$this->blacklisted
				|| 	(
							strlen($this->termBody) < $this->_SETTINGS['search']['min tag length']
						&& 	!$this->whitelisted
					)
			)
			{
				return $this->tagIds;
			}
			
			
			
			$this->tagIds = array($this->tagId);
		}
		// Else, if this term is the parent of OR terms...
		else
		{
			// For each of the child OR terms...
			for($i = 0; $i < count($this->childTerms); $i++)
			{
				// If this child term is actually an array of terms...
				if(is_array($this->childTerms[$i]))
				{
					// For each sub-child term...
					for($j = 0; $j< count($this->childTerms[$i]); $j++)
					{
						// Merge any existing item IDs with this term's item IDs.
						$this->tagIds = array_merge(is_array($this->tagIds) ? $this->tagIds : array(), is_array($this->childTerms[$i][$j]->tagIds) ? $this->childTerms[$i][$j]->tagIds : array());
					}
				}
				else
				{
					// Merge any existing item IDs with this term's item IDs.
					$this->tagIds = array_merge(is_array($this->tagIds) ? $this->tagIds : array(), is_array($this->childTerms[$i]->tagIds) ? $this->childTerms[$i]->tagIds : array());
				}
			}
		}
		
		
		
		// If this tag matches to any items...
		if(!empty($this->tagIds))
		{
			// Remove any duplicates.
			//$this->tagIds = array_unique($this->tagIds);
			
			// Reindex the array so we don't have any skipped indices.
			//$this->tagIds = array_values($this->tagIds);
		}
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
	}
	
	
	
	/**
	 * Retrieves the tag ID for this tag string.
	 */
	protected function GetTagId()
	{
		$tagIdQuery = 
			'SELECT 
				' . $this->tagTable['id column'] . '
			FROM 
				' . $this->tagTable['name'] . '
			WHERE 
				' . $this->tagTable['data column'] . ' = ?';
		
		$this->tagId = $this->DB->FetchAndFlatten($tagIdQuery, array($this->termBody));
		
		if(!$this->tagId || $this->tagId == null)
		{
			$this->Praesto->missingTag = true;
		}
	}
	
	
	
	/**
	 * Checks tag against the global blacklist.
	 *
	 * @param 	string 	$termString 		String of the term or tag skipped.
	 * @param 	string 	$reason 			Reason for being skipped.
	 * @param 	string 	$pass 				Phase in which it was skipped.
	 */
	protected function SkipTerm($termString, $reason, $pass)
	{
		// Add this term to the skipped array.
		$this->Praesto->QueryInterpreter->termsSkipped[] = array
		(
			'term' 		=> $termString,
			'reason' 	=> $reason,
			'pass'		=> $pass
		);
	}
	
}