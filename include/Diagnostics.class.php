<?php

class Diagnostics
{
	public $databases;
	
	protected $phpStartTime;
	protected $timers;
    
	// Default constructor.
    public function __construct()
    {
		$this->phpStartTime = $_SERVER['REQUEST_TIME_FLOAT'];
		
		$this->databases = array();
    }
	
	public function ExecutionTime()
	{
		return microtime(true) - $this->phpStartTime;
	}
	
	public function StartTimer()
	{
		$this->timers[] = microtime(true);
		
		return count($this->timers) - 1;
	}
	
	public function MeasureTimer($timerId)
	{
		return microtime(true) - $this->timers[$timerId];
	}
	
	public function DatabaseQueries()
	{
		$count = 0;
		
		if(!empty($this->databases))
		{
			foreach($this->databases as $db)
			{
				$count += $db->queryCount;
			}
		}
		
		return $count;
	}
	
	public function DatabaseCount()
	{
		return count($this->databases);
	}
	
	public function PeakMemory($real = false)
	{
		return memory_get_peak_usage($real);
	}
	
	public function Memory($real = false)
	{
		return memory_get_usage($real);
	}
	
	public function RenderReport()
	{
		echo
		'<table style="width:100%;background-color:rgba(220,220,220,0.8);margin-top:10px;border-radius:5px;padding:5px;float:left;border:1px solid rgb(60,60,60);text-align:center;font-size:80%;">
			<tr>
				<td>
					' . (number_format($this->ExecutionTime(), 4)) . 's
				</td>
				
				<td>
					' . $this->DatabaseQueries() . ' db queries
				</td>
				
				<td>
					' . $this->DatabaseCount() . ' db cnctions
				</td>
				
				<td>
					' . self::ReadableFileSize($this->PeakMemory()) . '
				</td>
			</tr>
		</table>';
	}
	
	static public function ReadableFileSize($size)
	{
		$unit = array('B','KiB','MiB','GiB','TiB','PiB');
		
		return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
	}
}