<?php

namespace Praesto;



/**
* Processes individual search terms.
*
* @package    Praesto
* @author     Christian Sibo
* @version    1.0
*/
class Term
{
	
	public $tagId;
	public $rawTerm;
	public $exclude;
	public $fuzzy;
	public $termBody;
	public $queryTerms;
	public $orTerms;
	public $database;
	public $taggedItems;
	
	protected $Search;
	protected $tagTable;
	protected $itemTable;
	protected $tagItemTable;
	protected $DB;
	
	private $_SETTINGS;
	
	
	
	/**
	 * Constructor.
	 *
	 * @param 	Praesto &$Search 	Reference to the Praesto main object.
	 * @param 	string 	$term 		Raw search string term.
	 */
	public function __construct(&$Search, $term)
	{
		global $_SETTINGS;
		
		$this->_SETTINGS = $_SETTINGS;
		
		$this->Search = $Search;
		
		
		
		// Grab the database key from the Praesto object.
		$this->database = $this->Search->database;
		
		// Get the settings for the database and its tables.
		$dbSettings = $this->_SETTINGS['databases'][$this->database]['database settings'];
		
		$this->itemTable 	= $dbSettings['item table'];
		$this->tagTable 	= $dbSettings['tag table'];
		$this->tagItemTable = $dbSettings['tag-item table'];
		
		
		
		// Get a new database connection.
		$this->DB = new \DatabaseHandler();
		$this->DB->Connect($this->database);
		
		
		
		// If the raw term is valid...
		if($this->SetTerm($term))
		{
			// Process the term to get the relevant item IDs.
			return $this->ProcessTerm();
		}
	}
	
	
	
	/**
	 * Sets the raw query term.
	 *
	 * @param 	string 	$term 	User's raw query term.
	 * @return 	bool 			True if success.
	 */
	public function SetTerm($term)
	{
		if
		(
			!empty($term)
			&& is_string($term)
		)
		{
			// Save the query term for later.
			$this->rawTerm = $term;
			
			// Return success.
			return true;
		}
		
		// Return failure.
		return false;
	}
	
	
	
	/**
	 * Processes the term to get the relevant item IDs.
	 *
	 * @return 	bool 			True if success.
	 */
	public function ProcessTerm()
	{
		// If the raw term isn't set...
		if(empty($this->rawTerm))
		{
			// Return failure.
			return false;
		}
		
		
		
		// Set up default values.
		$this->tagId 	= null;
		$this->exclude 	= false;
		$this->fuzzy 	= false;
		$this->orTerms 	= null;
		
		
		
		// Extract the components of the term.
		// 		$matches['exclude'] will only be set if the exclude token is the first character.
		// 		$matches['fuzzy'] will only be set if the fuzzy token is the first or second character.
		// 		$matches['body'] will contain the rest of the term.
		preg_match
		(
			'/^' . 
				'(?P<exclude>' . $this->_SETTINGS['terms']['tokens']['exclude'] . ')?' . 
				'(?P<fuzzy>' . $this->_SETTINGS['terms']['tokens']['fuzzy'] . ')?' . 
				'(?P<body>.+)' . 
			'$/', 
			
			$this->rawTerm, 
			$matches
		);
		
		
		
		// If the term was actually empty...
		if(empty($matches['body']))
		{
			// Return failure.
			return false;
		}
		
		
		
		// Set the exclude flag if the exclude token matched.
		$this->exclude = !empty($matches['exclude']);
		
		// Set the fuzzy flag if the fuzzy token matched.
		$this->fuzzy = !empty($matches['fuzzy']);
		
		// Save the term body.
		$this->termBody = $matches['body'];
		
		
		
		// If multi-word tags are stored in the table with a different delimiter than
		// is used in the raw query sting...
		if($this->_SETTINGS['databases'][$this->Search->database]['database settings']['tag table']['data is delimited'])
		{
			// Replace the raw multi-word delimiters with spaces.
			$this->termBody = str_replace($this->_SETTINGS['terms']['word delimiter'], ' ', $this->termBody);
		}
		
		
		
		// If exploding the term body on the Or token returns more than one element...
		if(		!preg_match('/' . $this->_SETTINGS['terms']['tokens']['query'] . '/', $this->termBody)
			&& 	count($exploded = explode($this->_SETTINGS['terms']['tokens']['or'], $matches['fuzzy'] . $this->termBody)) > 1)
		{
			// We need to process each ORed term as a new Term object.
			foreach($exploded as $subTerm)
			{
				$this->orTerms[] = new Term($this->Search, $subTerm);
			}
		}
		else
		{
			// If the fuzzy flag was set...
			if($this->fuzzy)
			{
				// Perform the fuzzy search.
				$this->GetFuzzyTerms();
			}
			
			
			
			// If the term looks like a regex...
			if($this->IsRegex())
			{
				// Perform the regex search.
				$this->GetRegExTerms();
			}
		}
		
		
		
		// We don't need this anymore.
		unset($matches);
		
		
		
		// If exploding the term body on the Query token returns one element...
		if(count($this->queryTerms = explode($this->_SETTINGS['terms']['tokens']['query'], $this->termBody)) == 1)
		{
			// Nullify the resultant array.
			$this->queryTerms = null;
		}
		// If exploding the term body on the Query token returns multiple elements...
		else
		{
			
			// Reassign the query terms to their respective indices.
			foreach($this->queryTerms as $index => $term)
			{
				// The position of the sub-elements determines how we process them.
				switch($index)
				{
					// Qualifying term.
					case 0:
					{
						$this->queryTerms['qualifier'] = $term;
						
						break;
					}
					
					// Term body.
					case 1:
					{
						$this->queryTerms['term'] = $term;
						
						if(count($exploded = explode($this->_SETTINGS['terms']['tokens']['or'], $this->queryTerms['term'])) > 1)
						{
							$this->queryTerms['term'] = $exploded;
						}
						
						break;
					}
					
					// Optional direction.
					case 2:
					{
						$this->queryTerms['direction'] = $term;
						
						break;
					}
				}
				
				// Remove the duplicate values.
				unset($this->queryTerms[$index]);
			}
		}
		
		
		
		// If this is a regular 'ol term...
		if(empty($this->orTerms) && empty($this->queryTerms))
		{
			// Get the tag ID associated with this tag string.
			$this->GetTagId();
			
			if
			(
					$this->Search->missingTag
				&&	$this->_SETTINGS['search']['fail on missing tag']
			)
			{
				return;
			}
		}
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
		
		
		
		// If this isn't a query command or filter...
		if(empty($this->queryTerms))
		{
			// Get the content tagged with this tag.
			$this->GetTaggedItems();
		}
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
		
		
		
		// Return success.
		return true;
	}
	
	
	
	/**
	 * Performs the fuzzy search on a term using the Damerau-Levenshtein distance.
	 */
	protected function GetFuzzyTerms()
	{
		// Will store the matching tags from the fuzzy search.
		$newTerms = array();
		
		
		
		// If we want to use the distance setting 
		// as a ratio of the raw term's length...
		if($this->_SETTINGS['search']['fuzzy search ratio'])
		{
			// The distance limit is the term's length over the distance setting.
			$limit = strlen($this->termBody) / $this->_SETTINGS['search']['max levenshtein distance'];
		}
		else
		{
			// The distance limit is the distance setting.
			$limit = $this->_SETTINGS['search']['max levenshtein distance'];
		}
		
		
		
		// For each tag in the table...
		foreach($this->Search->allTags as $compare)
		{
			// Get the Damerau-Levenshtein object.
			$DamerauLevenshtein = new \Oefenweb\DamerauLevenshtein\DamerauLevenshtein($this->termBody, $compare['tag']);
			
			// If the distance between the strings is <= the max distance...
			if($DamerauLevenshtein->getSimilarity() <= $limit)
			{
				// Create a new Term object with this tag and add it to our array.
				$newTerms[] = new Term($this->Search, $compare['tag']);
			}
			
			unset($DamerauLevenshtein);
		}
		
		
		
		// Add the new terms to the OR list, since they'll
		// be processed similarly later on.
		$this->orTerms[] = $newTerms;
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
	}
	
	
	
	/**
	 * Checks if a string is a valid regular expression.
	 *
	 * @return bool
	 */
	protected function IsRegex()
	{
		// Look for regex special tokens.
		if(preg_match('/[\.\+\{\}\[\]\?\|]+/', $this->termBody))
		{
			return true;
		}
		
		// Look for meta characters.
		if(preg_match('/\\\\\w/', $this->termBody))
		{
			return true;
		}
		
		return false;
	}
	
	
	
	/**
	 * Performs a regular expression search for tags.
	 */
	protected function GetRegExTerms()
	{
		// Will store the matching tags from the regex search.
		$newTerms = array();
		
		
		
		// For each tag in the table...
		foreach($this->Search->allTags as $compare)
		{
			if(preg_match('@^' . $this->termBody . '$@', $compare['tag']))
			{
				$newTerms[] = new Term($this->Search, $compare['tag']);
			}
		}
		
		
		
		// Add the new terms to the OR list, since they'll
		// be processed similarly later on.
		$this->orTerms[] = $newTerms;
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
	}
	
	
	
	/**
	 * Retrieves the item IDs that are tagged with this term's tag.
	 */
	public function GetTaggedItems()
	{
		// If this is an existing tag...
		if($this->tagId != null)
		{
			// Will store the IDs of the tagged items.
			$this->taggedItems = array();
			
			
			
			// If the term body is shorter than the minimum allowed...
			if
			(
					strlen($this->termBody) < $this->_SETTINGS['search']['min tag length']
				||	in_array($this->termBody, $this->_SETTINGS['terms']['blacklist'])
			)
			{
				return;
			}
			
			
			
			// Manually trigger garbage collector.
			Praesto::TriggerGC();
			
			
			
			// PDO query string to retrieve the item IDs from
			// the tag-item table for this tag ID.
			$itemTagIdsQuery = 
				'SELECT 
					' . $this->tagItemTable['item column'] . '
				FROM 
					' . $this->tagItemTable['name'] . '
				WHERE 
					' . $this->tagItemTable['tag column'] . ' = ?';
			
			// Fetch and flatten the PDO array, since we only want one column.
			$this->taggedItems = $this->DB->FetchAllAndFlatten($itemTagIdsQuery, array($this->tagId));
		}
		// Else, if this term is the parent of OR terms...
		else if(!empty($this->orTerms))
		{
			// For each of the child OR terms...
			foreach($this->orTerms as $setIndex => $orSet)
			{
				// If this child term is actually an array of terms...
				if(is_array($orSet))
				{
					// For each sub-child term...
					foreach($orSet as $termIndex => $orTerm)
					{
						// Merge any existing item IDs with this term's item IDs.
						$this->taggedItems = array_merge(is_array($this->taggedItems) ? $this->taggedItems : array(), is_array($orTerm->taggedItems) ? $orTerm->taggedItems : array());
						
						unset($this->orTerms[$setIndex][$termIndex]->taggedItems);
					}
				}
				else
				{
					// Merge any existing item IDs with this term's item IDs.
					$this->taggedItems = array_merge(is_array($this->taggedItems) ? $this->taggedItems : array(), is_array($orSet->taggedItems) ? $orSet->taggedItems : array());
					
					unset($this->orTerms[$setIndex]->taggedItems);
				}
			}
		}
		
		
		
		// If this tag matches to any items...
		if(!empty($this->taggedItems))
		{
			// Remove any duplicates.
			$this->taggedItems = array_unique($this->taggedItems);
			
			// Reindex the array so we don't have any skipped indices.
			$this->taggedItems = array_values($this->taggedItems);
		}
		
		
		
		// Manually trigger garbage collector.
		Praesto::TriggerGC();
	}
	
	
	
	/**
	 * Retrieves the tag ID for this tag string.
	 */
	protected function GetTagId()
	{
		$tagIdQuery = 
			'SELECT 
				' . $this->tagTable['id column'] . '
			FROM 
				' . $this->tagTable['name'] . '
			WHERE 
				' . $this->tagTable['data column'] . ' = ?';
		
		$this->tagId = $this->DB->FetchAndFlatten($tagIdQuery, array($this->termBody));
		
		if(!$this->tagId || $this->tagId == null)
		{
			$this->Search->missingTag = true;
		}
	}
	
}