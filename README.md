![logo_small.png](https://bitbucket.org/repo/zzep5j/images/3309183335-logo_small.png)

Praesto is a search engine written in PHP and MySQL that finds custom-tagged content such as images or blog posts within a database.

It is an exclusive search engine that allows searching for combinations and variations of tags to find very specific content. This method works best for large catalogs of data such as image boards and forums.

Capabilities:

* Inclusive and exclusive tags
* Fuzzy search (Damerau-Levenshtein string distance)
* RegEx search (PCRE syntax)
* OR search (Any combination of the above syntaxes)
* Query commands (ie. limit, offset, sort)
* Query filters (ie. score >= 10)

Features:

* Highly customizable to suit your needs.
* Made to work with multiple database structures.
* Built for both flexibility and speed.
* Easy to integrate into other search engines.

----

Check out Praesto's ** [Wiki](https://bitbucket.org/csibo/praesto/wiki/Home.wiki) ** for more information.  


## [Installation](https://bitbucket.org/csibo/praesto/wiki/Installation.wiki#!installation) ##


## [Usage](https://bitbucket.org/csibo/praesto/wiki/Usage.wiki#!usage) ##