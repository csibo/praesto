<?php

namespace Praesto;



// Include Praesto settings.
include('settings.php');

// External classes.
include_once('include/QueryInterpreter.class.php');
include_once('include/QueryTerm.class.php');
include_once('include/DatabaseHandler.class.php');
include_once('include/Diagnostics.class.php');
include_once('include/DamerauLevenshtein.class.php');



// Loose singleton of the Diagnostics class.
// Doesn't really need to be a singleton but
// there's no real reason to duplicate the objects.
if(!isset($Diagnostics))
{
	// Instantiate the Diagnostics class here since
	// other devs might not do so anywhere else.
	$Diagnostics = new \Diagnostics();
}



/**
* Praesto main class.
*
* @package    Praesto
* @author     Christian Sibo
* @version    1.0
*/
class Praesto
{
	
	public $QueryInterpreter;
	public $allTags;
	public $database;
	public $timeToIds;
	public $includeItemIds;
	public $limitOverride;
	
	protected $rawQuery;
	protected $DB;
	protected $tagTable;
	protected $itemTable;
	protected $tagItemTable;
	
	private $_SETTINGS;
	
	
	
	/**
	 * Constructor.
	 *
	 * @param 	string 	$database 	Database key from the settings file.
	 * @param 	string 	$query 		User's raw query string.
	 */
	public function __construct($database, $query)
	{
		global $_SETTINGS;
		
		$this->_SETTINGS = $_SETTINGS;
		
		
		
		// Get the settings for the database and its tables.
		$this->database = $database;
		
		$dbSettings = $this->_SETTINGS['databases'][$this->database]['database settings'];
		
		$this->itemTable 	= $dbSettings['item table'];
		$this->tagTable 	= $dbSettings['tag table'];
		$this->tagItemTable	= $dbSettings['tag-item table'];
		
		
		
		// Get a new database connection.
		$this->DB = new \DatabaseHandler();
		$this->DB->Connect($this->database);
		
		
		
		// Save the query string for later.
		$this->SetQuery($query);
	}
	
	
	
	/**
	 * Sets the raw query string.
	 *
	 * @param 	string 	$query 	User's raw query string.
	 * @return 	bool 			True if success.
	 */
	public function SetQuery($query)
	{
		// If the query string is valid...
		if
		(
			!empty($query) 
			&& is_string($query)
		)
		{
			// Save the query string for later.
			$this->rawQuery = $query;
			
			// Return success.
			return true;
		}
		
		// Return failure.
		return false;
	}
	
	
	
	/**
	 * Processes the raw query string into a list of search results.
	 *
	 * @return 	array 			PDO array of search results.
	 */
	public function ProcessQuery($limitOverride = false)
	{
		global $Diagnostics, $timer;
		
		
		
		$this->limitOverride = $limitOverride;
		
		
		
		new QueryInterpreter($this, $this->rawQuery);
		
		
		
		$this->GetItemIds();
		
		
		
		if(isset($Diagnostics))
		{
			$this->timeToIds = $Diagnostics->MeasureTimer($timer);
		}
		
		
		
		// Manually trigger garbage collector.
		self::TriggerGC();
	}
	
	
	
	/**
	 * Retrieves item IDs matching the query.
	 *
	 * @return 	array 						PDO array of item IDs.
	 */
	public function GetItemIds()
	{
		$this->includeItemIds = $this->QueryInterpreter->GetItemIds();
		
		return $this->includeItemIds;
	}
	
	
	
	/**
	 * Retrieves a list of all the tag strings. Loaded only when there are fuzzy or regex terms.
	 */
	public function GetAllTags()
	{
		if(empty($this->allTags))
		{
			// Retrieve all of the tags from the tags table.
			// We'll do this once here so we don't make the terms do this with a bunch of SQL queries later.
			$this->allTags = $this->DB->FetchAllAndFlatten('SELECT ' . $this->tagTable['data column'] . ' FROM ' . $this->tagTable['name']);
		}
	}
	
	
	
	static public function TriggerGC()
	{
		// Manually trigger garbage collector.
		gc_collect_cycles();
	}
	
}