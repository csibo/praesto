<?php

// Include the base class.
include_once('include/DatabaseHandler.class.php');
require_once(__DIR__.'/settings.php');

$database = 'big';

$dbSettings		= $_SETTINGS['databases'][$database]['database settings'];

$itemTable 		= $dbSettings['item table'];
$tagTable 		= $dbSettings['tag table'];
$tagItemTable	= $dbSettings['tag-item table'];

$DB = new \DatabaseHandler();
$DB->Connect($database);

$numberOfItems = 5000000;
$tagsPerItem = 2; // num of tags available / this = tags that will be added.

$allTags = $DB->FetchAll('SELECT * FROM ' . $tagTable['name'] . ' WHERE 1');

$tagsToApply = count($allTags) / $tagsPerItem;

$usernames = array
(
	'bob',
	'jane',
	'joe',
	'jim',
	'jake',
	'john',
	'jeff'
);


$DB->Execute('TRUNCATE TABLE ' . $itemTable['name']);
$DB->Execute('TRUNCATE TABLE ' . $tagItemTable['name']);

for($i = 0; $i < $numberOfItems; $i++)
{
	set_time_limit(300);
	
	$data 	= bin2hex(mcrypt_create_iv(11, MCRYPT_DEV_URANDOM));
	$score 	= rand(-100, 100);
	$user 	= $usernames[rand(0, count($usernames) - 1)];
	
	$query = 
	'INSERT INTO 
		' . $itemTable['name'] . ' 
		(data, score, user)
	VALUES
		(?, ?, ?)';
		
	$DB->Execute($query, array($data, $score, $user));
	
	$id = $DB->LastInsertId();
	
	
	$tagsApplied = array();
	
	for($x = 0; $x < $tagsToApply; $x++)
	{
		do
		{
			$tag = $allTags[rand(1, count($allTags))]['id'];
			
		}while(in_array($tag, $tagsApplied));
		
		$tagsApplied[] = $tag;
		
		$query = 
		'INSERT INTO 
			' . $tagItemTable['name'] . ' 
			(item_id, tag_id)
		VALUES
			(?, ?)';
			
		$DB->Execute($query, array($id, $tag));
	}
}	