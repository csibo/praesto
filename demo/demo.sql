-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2015 at 09:08 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tag_search`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `data` varchar(100) NOT NULL COMMENT 'Example column',
  `score` smallint(11) NOT NULL COMMENT 'Example column',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tagged data (ie. Images, posts, etc).' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `data`, `score`) VALUES
(1, 'Mario', 5),
(2, 'Luigi', 7),
(3, 'Peach', -2),
(4, 'Toad', 4),
(5, 'Goomba', 0),
(6, 'Yoshi', 10),
(7, 'Peach and Toad', 14);

-- --------------------------------------------------------

--
-- Table structure for table `item_tags`
--

CREATE TABLE IF NOT EXISTS `item_tags` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `item_id` int(20) unsigned NOT NULL COMMENT 'Primary key of the row in the "item" table',
  `tag_id` mediumint(20) unsigned NOT NULL COMMENT 'Primary key of the row in the "tags" table',
  PRIMARY KEY (`id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Relationship between tags and tagged items.' AUTO_INCREMENT=17 ;

--
-- Dumping data for table `item_tags`
--

INSERT INTO `item_tags` (`id`, `item_id`, `tag_id`) VALUES
(1, 1, 3),
(2, 1, 7),
(3, 1, 5),
(4, 2, 7),
(5, 2, 3),
(6, 2, 6),
(7, 3, 8),
(8, 3, 3),
(9, 3, 2),
(10, 1, 2),
(11, 2, 2),
(12, 6, 1),
(13, 7, 8),
(14, 6, 7),
(15, 7, 1),
(16, 7, 7);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` mediumint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `tag` varchar(100) NOT NULL COMMENT 'Tag value',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`tag`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='List of all possible tags.' AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag`) VALUES
(7, '1boy'),
(8, '1girl'),
(9, '2boys'),
(10, '2girls'),
(3, 'artist request'),
(5, 'censored'),
(2, 'tagme'),
(1, 'test'),
(4, 'translation request'),
(6, 'uncensored');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
