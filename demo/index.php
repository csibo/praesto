<?php

// Include the base class.
include_once('../Praesto.class.php');

include_once('../include/Diagnostics.class.php');



if(!isset($Diagnostics))
{
	$Diagnostics = new \Diagnostics();
}



// Retrieve the query string.
$query = $_POST['q'];

// Retrieve the database requested.
$db = $_POST['db'];
		
		
		
// Get the settings for the database and its tables.
$database = $db ;

$dbSettings = $_SETTINGS['databases'][$database]['database settings'];

$itemTable 		= $dbSettings['item table'];
$tagTable 		= $dbSettings['tag table'];
$tagItemTable	= $dbSettings['tag-item table'];



// Get a new database connection.
$DB = new \DatabaseHandler();
$DB->Connect($database);



$timer = $Diagnostics->StartTimer();

$memory = $Diagnostics->Memory();



// Initialize the Praesto object.
$Praesto = new Praesto\Praesto($db, $query);

// Process the query string into the resultant items.
$Praesto->ProcessQuery();

$ids = $Praesto->includeItemIds;

$memory = abs($memory - $Diagnostics->PeakMemory());

$timeInPraesto = $Diagnostics->MeasureTimer($timer);



$timer = $Diagnostics->StartTimer();

$result = $DB->FetchAll('SELECT * FROM ' . $itemTable['name'] . ' WHERE ' . $itemTable['id column'] . ' IN (' . implode(',', $ids) . ') ORDER BY FIELD(' . $itemTable['id column'] . ',' . implode(',', $ids) . ')');

$timetoFetch = $Diagnostics->MeasureTimer($timer);


$timings = array
(
	array
	(
		'metric' => 'Time in Praesto',
		'value' => number_format($timeInPraesto, 4) . 's',
	),
	
	array
	(
		'metric' => 'Time to find all tagged item IDs',
		'value' => number_format($Praesto->timeToIds, 4) . 's',
	),
	
	array
	(
		'metric' => 'Time to fetch all tagged items',
		'value' => number_format($timetoFetch, 4) . 's',
	),
	
	array
	(
		'metric' => 'Peak memory in Praesto',
		'value' => \Diagnostics::ReadableFileSize($memory),
	),
);


$dbStats = array
(
	array
	(
		'metric' => 'Number of Items',
		'value' => '~' . number_format($DB->FetchAndFlatten('SELECT TABLE_ROWS FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "' . $itemTable['name'] . '"')),
	),
	
	array
	(
		'metric' => 'Number of Tags',
		'value' => '~' . number_format($DB->FetchAndFlatten('SELECT TABLE_ROWS FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "' . $tagTable['name'] . '"')),
	),
	
	array
	(
		'metric' => 'Number of Tag-Item Relations',
		'value' => '~' . number_format($DB->FetchAndFlatten('SELECT TABLE_ROWS FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "' . $tagItemTable['name'] . '"')),
	),
);

?>



<!DOCTYPE html>
<html>

	<head>
	
		<title>Praesto - Demo</title>
		
		<meta charset="utf-8">
		
		<link rel="stylesheet" href="assets/css/main.css" type="text/css" />
	
	</head>
	
	
	
	<body>
	
		<div class="home">

			<img src="https://bitbucket.org/repo/zzep5j/images/3309183335-logo_small.png" style="height:200px;margin:0 auto;display:block;">
			
			<br>
			<br>

			<div class="homeSearchWrapper">
			
				<form method="POST" style="margin:0 auto;display:block;text-align:left;">

					<input type="text" name="q" id="q" class="searchInput" value="<?=$query?>" style="width:60%;margin:0;padding:0;padding-left:5px;height:30px;font-size:20px;" placeholder="Query">

					<input type="submit" value="Search" style="width:20%;margin:0;padding:0;height:30px;font-size:20px;">

					<select name="db" id="db" style="width:18%;margin:0;padding:0;height:30px;font-size:20px;text-align:center;">
					
<?php 					foreach($_SETTINGS['databases'] as $index => $dbSetting):	?>
					
							<option value="<?=$index?>"<?=($index == $db ? ' selected' : '')?>>
							
								<?=ucwords(str_replace('_', ' ', $index))?>
								
							</option>
					
<?php 					endforeach	?>
					
					</select>
					
				</form>
			
			</div>
			
			
			
<?php		if(!empty($query)):	?>

				<div class="resultsBox">

					<h2>Performance</h2>
					
					<h3>Global</h3>
					
					<?=$Diagnostics->RenderReport()?>
					
					<br>
					<br>
					<br>
					
					<h3>Praesto</h3>
					
					<?=DatabaseHandler::RenderAsTable($timings)?>

					<br>
					
					<h3>Database</h3>
					
					<?=DatabaseHandler::RenderAsTable($dbStats)?>

					<br>
					<br>
					<br>

					<h2>Results (<?=($result ? count($result) : 0)?>)</h2>
					
					<?=DatabaseHandler::RenderAsTable($result, 10)?>

					<br>
					<br>
					<br>

					<div style="width:100%;">
					
						<div style="width:50%;display:inline-block;">
						
							<h2>Terms Found (<?=count($Praesto->QueryInterpreter->termsFound)?>)</h2>
							
							<?=DatabaseHandler::RenderAsTable($Praesto->QueryInterpreter->termsFound)?>
						
						</div>
					
						<div style="width:49%;display:inline-block;">

							<h2>Terms Processed (<?=count($Praesto->QueryInterpreter->termsProcessed)?>)</h2>
							
							<?=DatabaseHandler::RenderAsTable((array)$Praesto->QueryInterpreter->termsProcessed)?>
						
						</div>
					
					</div>

					<br>
					<br>
					<br>

					<div style="width:100%;">
					
						<div style="width:50%;display:inline-block;">

							<h2>Terms Included (<?=count($Praesto->QueryInterpreter->tagsIncluded)?>)</h2>
							
							<?=DatabaseHandler::RenderAsTable($Praesto->QueryInterpreter->tagsIncluded)?>
						
						</div>
					
						<div style="width:49%;display:inline-block;">

							<h2>Terms Excluded (<?=count($Praesto->QueryInterpreter->tagsExcluded)?>)</h2>
							
							<?=DatabaseHandler::RenderAsTable($Praesto->QueryInterpreter->tagsExcluded)?>
						
						</div>
					
					</div>

					<br>
					<br>
					<br>

					<h2>Terms Skipped (<?=count($Praesto->QueryInterpreter->termsSkipped)?>)</h2>
					
					<?=DatabaseHandler::RenderAsTable($Praesto->QueryInterpreter->termsSkipped)?>

					<br>
					<br>
					<br>

					<div style="width:100%;">
					
						<div style="width:50%;display:inline-block;">

							<h2>Query Commands (<?=count($Praesto->QueryInterpreter->queryCommands)?>)</h2>
							
							<?=DatabaseHandler::RenderAsTable($Praesto->QueryInterpreter->queryCommands)?>
						
						</div>
					
						<div style="width:49%;display:inline-block;">

							<h2>Query Filters (<?=count($Praesto->QueryInterpreter->queryFilters)?>)</h2>
							
							<?=DatabaseHandler::RenderAsTable($Praesto->QueryInterpreter->queryFilters)?>
						
						</div>
					
					</div>
				
				</div>
			
<?php		endif	?>
			
		</div>
		
	</body>
	
</html>